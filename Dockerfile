FROM buildpack-deps:buster

WORKDIR /usr/local/bin

COPY server .
CMD ["./server"]
